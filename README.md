# House Party 2.0

Download/clone this repository and open `Scenes/Island.unity` to play the game.

For the 2D Fall 2015 version, checkout the `fall2015-final` tag and open `Scenes/Game.unity`.

## Microphone setup (Windows)

Microphone input doesn't work on Mac OS because Unity on Mac can't read from multiple mic inputs simultaneously. See [this thread](http://forum.unity3d.com/threads/record-multiple-mics-simultaneously-mac-issue.357143/) for more info.

Enable the "Mic Manager" GameObject and uncheck Game Manager's "Use Mouse Input" option.

The microphone input names for each direction are specified on the "Mic Manager" GameObject. When testing inputs, if microphones are switched, switch the names of the microphones on the individual "Mic Source" scripts.

### Kinect setup (Windows)

1. Download and install the Kinect 1.8 SDK from [here](https://www.microsoft.com/en-us/download/details.aspx?id=40278)
1. Build and run the "KinectAudioServer" project in the root of this repo
1. Follow the calibration instructions. Press enter after each step.
1. Make sure the "Kinect Audio Client" component on "Mic Manager" is enabled in the game

When running and connected to Unity, the tracked position should start appearing in the KinectAudioServer output.

## Audio visualization setup

### Windows

1. Download [Virtual Audio Cable](http://software.muzychenko.net/eng/vac.htm). Look for the full download link in the House Party group.
1. Download AudioRouter from [this thread](https://www.reddit.com/r/software/comments/3f3em6/is_there_a_alternative_to_chevolume/) ([mirror](https://drive.google.com/open?id=0B9SDbMllk3_KMmt4LWFOQkJBeVU))
1. Open the "Virtual Audio Cable Control Panel"
1. Inside AudioRouter, assign Spotify to "Line 1" output

### Mac OS

This can work on Mac OS, but only if the other microphone inputs are disabled.
Use [Loopback](https://rogueamoeba.com/loopback/) instead to route the audio from Spotify to Unity, and change the "Microphone Name" field on the "Audio Waveform" GameObject's "Audio Source from Microphone" component to the name of the Loopback audio source.
