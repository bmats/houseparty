﻿using System;
using System.Drawing;
using Microsoft.Kinect;
using Emgu.CV;
using Emgu.CV.Cvb;
using Emgu.CV.Structure;

namespace KinectAudioServer {
    class FrameProcessor {
        public event EventHandler<SoundPositionEventArgs> SoundPosition;

        private const string PreviewWindow = "Preview Window";
        private const int MinTargetSize = 90;
        private const long UpdateInterval = 1500000; // 10000 ticks per ms

        private KinectSensor _sensor;
        private long LastUpdateTime;

        private int _calibrateStep = 0;
        private PointF _topLeft, _bottomLeft, _topRight, _bottomRight;
        private PointF _left, _right, _top, _bottom;
        private PointF _position;

        private byte[] _pixelData;
        private Image<Gray, UInt16> _image;
        private Image<Gray, byte> _byteImage, _binaryImage, _dilateImage;
        private CvBlobDetector _blobDetector = new CvBlobDetector();
        private CvBlobs _blobs = new CvBlobs();

        public FrameProcessor(KinectSensor sensor) {
            _sensor = sensor;
            _sensor.ColorStream.Enable(ColorImageFormat.InfraredResolution640x480Fps30);
            _pixelData = new byte[_sensor.ColorStream.FramePixelDataLength];
            _image = new Image<Gray, UInt16>(640, 480);

            _sensor.ColorFrameReady += sensor_ColorFrameReady;
        }

        public void Calibrate() {
            if (_calibrateStep > 0) {
                return;
            }

            _calibrateStep = 1;

            Console.Write("Hold marker in top left corner. ");
            Console.ReadLine();
            Console.WriteLine("({0}, {1})", _topLeft.X, _topLeft.Y);
            ++_calibrateStep;

            Console.Write("Hold marker in top right corner. ");
            Console.ReadLine();
            Console.WriteLine("({0}, {1})", _topRight.X, _topRight.Y);
            ++_calibrateStep;

            Console.Write("Hold marker in bottom right corner. ");
            Console.ReadLine();
            Console.WriteLine("({0}, {1})", _bottomRight.X, _bottomRight.Y);
            ++_calibrateStep;

            Console.Write("Hold marker in bottom left corner. ");
            Console.ReadLine();
            Console.WriteLine("({0}, {1})", _bottomLeft.X, _bottomLeft.Y);

            _calibrateStep = 0;
            //CvInvoke.DestroyAllWindows();

            _left.X   = (_topLeft.X    + _bottomLeft.X)  * 0.5f;
            _left.Y   = (_topLeft.Y    + _bottomLeft.Y)  * 0.5f;
            _right.X  = (_topRight.X   + _bottomRight.X) * 0.5f;
            _right.Y  = (_topRight.Y   + _bottomRight.Y) * 0.5f;
            _top.X    = (_topLeft.X    + _topRight.X)    * 0.5f;
            _top.Y    = (_topLeft.Y    + _topRight.Y)    * 0.5f;
            _bottom.X = (_bottomLeft.X + _bottomRight.X) * 0.5f;
            _bottom.Y = (_bottomLeft.Y + _bottomRight.Y) * 0.5f;

            Console.WriteLine("Done!");
        }

        private void SaveCalibration(PointF point) {
            switch (_calibrateStep) {
            case 1:
                _topLeft = point;
                break;
            case 2:
                _topRight = point;
                break;
            case 3:
                _bottomRight = point;
                break;
            case 4:
                _bottomLeft = point;
                break;
            }
        }

        private void sensor_ColorFrameReady(object sender, ColorImageFrameReadyEventArgs e) {
            long now = DateTime.Now.Ticks;
            if (now - LastUpdateTime < UpdateInterval) return;
            LastUpdateTime = now;

            using (var frame = e.OpenColorImageFrame()) {
                frame.CopyPixelDataTo(_pixelData);

                _blobs.Clear();
                _image.Bytes = _pixelData;
                _byteImage = _image.Convert<Gray, byte>();
                _binaryImage = _byteImage.ThresholdBinary(new Gray(100), new Gray(255));
                _dilateImage = _binaryImage.Dilate(1);

                _blobDetector.Detect(_dilateImage, _blobs);
                _dilateImage.Dispose();
                _dilateImage = null;
                _byteImage.Dispose();
                _byteImage = null;

                int maxArea = 0;
                CvBlob maxBlob = null;
                foreach (var blob in _blobs) {
                    if (blob.Value.Area > maxArea) {
                        _position = blob.Value.Centroid;
                        maxBlob = blob.Value;
                        maxArea = blob.Value.Area;
                    }
                }

                if (maxBlob != null) {
                    // Show preview window with blob rectangle
                    CvInvoke.NamedWindow(PreviewWindow);
                    CvInvoke.Rectangle(_binaryImage, maxBlob.BoundingBox, new MCvScalar(255, 255, 255), 1);
                    CvInvoke.Imshow(PreviewWindow, _binaryImage);
                    CvInvoke.WaitKey(1); // event loop
                }
                _binaryImage.Dispose();
                _binaryImage = null;

                if (_calibrateStep > 0) {
                    SaveCalibration(_position);
                } else if (maxArea > MinTargetSize) {
                    // Report position
                    _position.X = RelativeDistance(ref _position, ref _left, ref _right);
                    _position.Y = RelativeDistance(ref _position, ref _bottom, ref _top);
                    SoundPosition?.Invoke(this, new SoundPositionEventArgs(_position.X, _position.Y));
                }
            }
        }

        private static float RelativeDistance(ref PointF pos, ref PointF a, ref PointF b) {
            PointF pointVec = new PointF(pos.X - a.X, pos.Y - a.Y);
            PointF fullVec  = new PointF(b.X - a.X, b.Y - a.Y);
            float pointDotFull = pointVec.X * fullVec.X + pointVec.Y * fullVec.Y;
            float fullDotFull  = fullVec.X * fullVec.X + fullVec.Y * fullVec.Y;
            return pointDotFull / fullDotFull;
        }
    }

    class SoundPositionEventArgs : EventArgs {
        public float X { get; private set; }
        public float Y { get; private set; }

        public SoundPositionEventArgs(float x, float y) {
            this.X = x;
            this.Y = y;
        }
    }
}
