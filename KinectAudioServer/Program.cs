﻿using System;
using System.Linq;
using System.Threading;
using Microsoft.Kinect;

namespace KinectAudioServer {
    class Program {
        static void Main(string[] args) {
            if (KinectSensor.KinectSensors.Count < 1) {
                Console.Error.WriteLine("No Kinects connected.");
                return;
            }

            var sensor = KinectSensor.KinectSensors.First(s => s.Status == KinectStatus.Connected);
            sensor.Start();

            var proc = new FrameProcessor(sensor);
            proc.Calibrate();

            var server = new AudioServer();
            proc.SoundPosition += (sender, e) => {
                server.Send(e.X, e.Y);
                Console.WriteLine("{0}\t{1}", e.X, e.Y);
            };

            // Wait to be killed
            var cancelEvent = new AutoResetEvent(false);
            Console.CancelKeyPress += (sender, e) => cancelEvent.Set();
            cancelEvent.WaitOne();

            Console.WriteLine("Bye");
            server.Stop();
            sensor.Stop();
        }
    }
}
