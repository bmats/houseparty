﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace KinectAudioServer {
    class AudioServer {
        private TcpListener _server;
        private TcpClient _client;
        private NetworkStream _stream;

        public AudioServer() {
            Start();
        }

        private void Start() {
            _server = new TcpListener(IPAddress.Loopback, 3000);
            _server.Start();

            var endpoint = (IPEndPoint) _server.LocalEndpoint;
            Console.WriteLine("Started server on {0}:{1}", endpoint.Address, endpoint.Port);

            _client = _server.AcceptTcpClient();
            Console.WriteLine("Connected");

            _stream = _client.GetStream();
        }

        public void Stop() {
            _stream.Close();
            _stream = null;
            _client.Close();
            _server.Stop();
        }

        public void Send(double x, double y) {
            if (_stream == null) return;

            string str = $"{x} {y}\n";
            byte[] buf = Encoding.ASCII.GetBytes(str);

            try {
                _stream.Write(buf, 0, buf.Length);
            } catch (IOException e) {
                Console.Error.WriteLine(e);
                Stop();

                Console.WriteLine("Restarting server...");
                Start();
            }
        }
    }
}
