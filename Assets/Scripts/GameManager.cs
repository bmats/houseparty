﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
    public static int Score = 0;

    public static GameManager Instance { get; private set; }

    public float GameTime = 90.0f;
    public Text ScoreText;
    public Text TimeText;
    public bool Intro = false;
    public float IntroDuration = 8f;
    public GameObject IntroCameraRoot = null;
    public GameObject GameOverMessage;
    public GameObject[] GameOverFireworks;
    public Light GameOverSpotlightPrefab;
    public int GameOverSpotlightCount = 8; // corresponds to pixel light count in render settings
    public Transform GameOverCameraTransform;
    public float GameOverCameraTransitionTime = 2f;
    public bool UseMouseInput = false;

    public static bool GameOver { get; set; }

    void Start() {
        Instance = this;

        if (Intro) {
            ShowIntro();
        }
    }

    public Vector3 SoundPosition {
        get {
            // bottom-left (0, 0) to top-right (1, 1)
            Vector3 position = UseMouseInput
                ? Input.mousePosition
                : new Vector3(manager.x, manager.y, 0);

            position.x /= Camera.main.pixelWidth;
            position.y /= Camera.main.pixelHeight;
            return position;
        }
    }

    void Update() {
        ScoreText.text = Score.ToString();

        int time = (int)Mathf.Ceil(GameTime - Time.timeSinceLevelLoad);
        if (time < 0) time = 0;
        TimeText.text = time.ToString();

        if (!GameOver && Time.timeSinceLevelLoad > GameTime) {
            GameOver = true;

            ShowGameOver();
        }

        if (GameOver) {
            // Allow space to restart
            if (Input.GetKey(KeyCode.Space)) {
                Restart();
            }
        }
    }

    public void Restart() {
        Score = 0;
        GameOver = false;

        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex); // reload
    }

    private void ShowIntro() {
        GameTime += IntroDuration;
        GameOver = true; // pause everything

        IntroCameraRoot.SetActive(true);
        Camera introCam = IntroCameraRoot.GetComponentInChildren<Camera>();

        LeanTween.rotateAroundLocal(IntroCameraRoot, Vector3.up, 360.0f, IntroDuration)
            .setEase(LeanTweenType.easeOutSine);
        LeanTween.moveLocal(introCam.gameObject, Vector3.zero, IntroDuration)
            .setEase(LeanTweenType.easeOutSine);
        LeanTween.rotateLocal(introCam.gameObject, Camera.main.transform.eulerAngles, IntroDuration)
            .setEase(LeanTweenType.easeOutSine)
            .setOnComplete(() => {
                IntroCameraRoot.SetActive(false);
                GameOver = false; // unpause
            });
    }

    private void ShowGameOver() {
        GameOverMessage.SetActive(true);

        foreach (var obj in GameOverFireworks) {
            obj.SetActive(true);
        }

        LeanTween.move(Camera.main.gameObject, GameOverCameraTransform.position, GameOverCameraTransitionTime)
            .setEase(LeanTweenType.easeInOutQuad);
        LeanTween.rotate(Camera.main.gameObject, GameOverCameraTransform.eulerAngles, GameOverCameraTransitionTime)
            .setEase(LeanTweenType.easeInOutQuad);

        for (int i = 0; i < GameOverSpotlightCount; ++i) {
            Instantiate(GameOverSpotlightPrefab,
                GameOverSpotlightPrefab.transform.position, Quaternion.identity);
        }
    }
}
