using UnityEngine;

[RequireComponent(typeof (AudioSource))]
class AudioSourceFromMicrophone : MonoBehaviour {
    public string MicrophoneName = null;

    void Start() {
        // foreach (string dev in Microphone.devices) Debug.Log(dev);
        var source = GetComponent<AudioSource>();
        source.clip = Microphone.Start(MicrophoneName, false, 999, AudioSettings.outputSampleRate);
        source.Play();
    }
}
