﻿using UnityEngine;

public class Building : MonoBehaviour {
    public int Occupants = 10;
    public Transform Person;
    public Transform DoorPosition;
    public AgentSpawner Spawner;
    public float Interval = 0.3f;

    private bool _triggered = false;
    private float _lastSpawnTime = 0;

    void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag == "Sound Zone") {
            _triggered = true;
            //Debug.Log ("Entered a building");

            // Make highlights
            Behaviour halo = (Behaviour) GetComponent("Halo");
            if (halo != null) {
                halo.enabled = true;
            }
        }
    }

    void OnTriggerExit(Collider other) {
        if (other.gameObject.tag == "Sound Zone") {
            _triggered = false;
            //Debug.Log ("Exited a building");

            // Hide highlights
            Behaviour halo = (Behaviour) GetComponent("Halo");
            if (halo != null) {
                halo.enabled = false;
            }
        }
    }

    void Update() {
        if (_triggered) {
            // Only spawn a person every interval
            if (Occupants > 0 && Time.time - _lastSpawnTime > Interval) {
                Transform agent = Spawner.NewAgent(Person);
                agent.position = DoorPosition.position;
                agent.GetComponent<PartygoerTrigger>().Trigger(); // go to party!
                --Occupants;

                _lastSpawnTime = Time.time;
            }
        }
    }
}
