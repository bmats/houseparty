﻿using UnityEngine;

public class LookAtCamera : MonoBehaviour {
    void Update() {
        transform.LookAt(Camera.main.transform);
         // Point up toward camera instead of forward
        transform.Rotate(90, 0, 0);
	}
}
