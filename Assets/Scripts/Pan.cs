﻿using UnityEngine;

public class Pan : MonoBehaviour {
    public float PanDistance = 1.0f;

    public float SwitchMargin = 0.1f;
    public float SwitchDelay = 2f;
    public float RotateTime = 1f;

    private Vector3 _startRotation;

    private float _sideEnterTime = 0;
    private bool _isSwitching = false;

    void Start() {
        _startRotation = transform.eulerAngles;
    }

    void Update() {
        if (GameManager.GameOver) return;

        float x = GameManager.Instance.SoundPosition.x;

        if (x < SwitchMargin || x > 1 - SwitchMargin) {
            // In switch margin
            if (!_isSwitching) {
                if (_sideEnterTime <= 0) _sideEnterTime = Time.time;

                // Wait a bit before actually switching
                if (Time.time - _sideEnterTime > SwitchDelay) {
                    Vector3 rotation = new Vector3(0, x > 0.5f ? 90 : -90, 0);
                    _isSwitching = true;

                    LeanTween.rotate(gameObject, transform.eulerAngles + rotation, RotateTime)
                        .setEase(LeanTweenType.easeInOutCubic)
                        .setOnComplete(() => {
                            // Done switching
                            _isSwitching = false;
                            _sideEnterTime = 0;
                            _startRotation = transform.eulerAngles;
                        });
                }
            }
        }

        // Pan slightly when aiming around
        if (!_isSwitching) {
            transform.eulerAngles = _startRotation + new Vector3(0, (x - 0.5f) * 2 * PanDistance, 0);
        }
    }
}
