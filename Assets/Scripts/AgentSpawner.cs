﻿using UnityEngine;

public class AgentSpawner : MonoBehaviour {
    [System.Serializable]
    public struct Region {
        public int Count;
        public int EnemyCount;
        public MeshFilter NavMesh;
        public Transform Agent;
        public Transform EnemyAgent;
    }

    public Region[] Regions;

	void Start() {
        foreach (var region in Regions) {
            var mesh = region.NavMesh.mesh;

            for (int i = 0; i < region.Count; ++i) {
                Transform agent = NewAgent(region.Agent);
                agent.position = GetRandomPointOnMesh(mesh, region.NavMesh.transform);
            }

            for (int i = 0; i < region.EnemyCount; ++i) {
                Transform enemyAgent = NewAgent(region.EnemyAgent);
                enemyAgent.position = GetRandomPointOnMesh(mesh, region.NavMesh.transform);
            }
        }
	}

    public Transform NewAgent(Transform agent) {
        Transform newAgent = Object.Instantiate(agent);
        newAgent.parent = transform;
        return newAgent;
    }

    private Vector3 GetRandomPointOnMesh(Mesh mesh, Transform transform) {
        // Get a random triangle and its verts
        int triIndex = (int)(Random.value * mesh.triangles.Length / 3) * 3;
        Vector3 a = mesh.vertices[mesh.triangles[triIndex]];
        Vector3 b = mesh.vertices[mesh.triangles[triIndex + 1]];
        Vector3 c = mesh.vertices[mesh.triangles[triIndex + 2]];

        // See https://math.stackexchange.com/questions/538458/triangle-point-picking-in-3d
        Vector3 point = a + Random.value * (b - a) + Random.value * (c - a);
        return transform.TransformPoint(point);
    }
}
