﻿using UnityEngine;

public class SunRotate : MonoBehaviour {
    public float EndAngle = 180f;

    private float _startAngle;

    void Start() {
        _startAngle = transform.eulerAngles.x;
    }

    void Update() {
        if (GameManager.GameOver) {
            return;
        }

        transform.rotation *= Quaternion.Euler((EndAngle - _startAngle) * Time.deltaTime / GameManager.Instance.GameTime, 0, 0);
    }
}
