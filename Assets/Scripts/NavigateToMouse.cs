﻿using UnityEngine;

[RequireComponent(typeof(NavMeshAgent))]
public class NavigateToMouse : MonoBehaviour {
    private NavMeshAgent _agent;

	void Start() {
	   _agent = GetComponent<NavMeshAgent>();
	}

	void Update() {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit = new RaycastHit();
        if (Physics.Raycast(ray, out hit, 1000)) {
	       _agent.destination = hit.point;
        }
	}
}
