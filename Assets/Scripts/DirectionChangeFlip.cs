﻿using UnityEngine;

[RequireComponent(typeof(NavMeshAgent))]
public class DirectionChangeFlip : MonoBehaviour {
    public Transform Graphics;

    private NavMeshAgent _agent;
    private bool _isFlipped = false;

	void Start() {
	   _agent = GetComponent<NavMeshAgent>();
	}

	void Update() {
	   bool flip = _agent.velocity.x > 0;

       if (flip != _isFlipped) {
           Vector3 newScale = Graphics.localScale;
           newScale.x *= -1;
           Graphics.localScale = newScale;

           _isFlipped = flip;
       }
	}
}
