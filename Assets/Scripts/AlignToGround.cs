﻿using UnityEngine;

public class AlignToGround : MonoBehaviour {
    void Update() {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, Vector3.down, out hit, 10)) {
            transform.rotation = Quaternion.FromToRotation(Vector3.up, hit.normal) *
                Quaternion.Euler(0, transform.eulerAngles.y, 0);
        }
    }
}
