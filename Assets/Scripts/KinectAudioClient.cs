﻿using UnityEngine;
using System.Net;
using System.Net.Sockets;
using System.Text;

public class KinectAudioClient : MonoBehaviour {
    const int ServerPort = 3000;

    public Vector2 CurrentPosition = Vector2.zero;

    private NetworkStream _stream = null;
    private byte[] buf = new byte[256];

    void Start() {
        var client = new TcpClient(new IPEndPoint(IPAddress.Loopback, 3001));
        client.BeginConnect(IPAddress.Loopback, ServerPort, rc => {
            if (client.Connected) {
                Debug.Log("Connected to Kinect Audio server");
                _stream = client.GetStream();
            } else {
                Debug.LogWarning("Could not connect to Kinect Audio server");
            }
        }, client);
    }

    void FixedUpdate() {
        if (_stream == null || !_stream.DataAvailable) return;

        // Parse line starting from the most recent going backwards
        _stream.Read(buf, 0, buf.Length);
        var str = Encoding.ASCII.GetString(buf);
        var lines = str.Split('\n');
        for (int i = lines.Length - 1; i >= 0; --i) {
            if (lines[i].Length == 0) continue;

            var values = lines[i].Split(' ');
            if (values.Length != 2) continue;

            float x, y;
            if (!float.TryParse(values[0], out x)) continue;
            if (!float.TryParse(values[1], out y)) continue;

            // Output in inspector
            CurrentPosition.x = x;
            CurrentPosition.y = y;

            manager.horizontalPosition = x;
            manager.verticalPosition = y;
            break;
        }
    }
}
