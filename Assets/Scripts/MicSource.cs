using UnityEngine;

public class MicSource : MonoBehaviour {
    const int SampleRate = 44100;

    public enum MicType {
        Top, Bottom, Left, Right
    }

    public string Device;
    public MicType Type;

    public float TempTotal;

    private AudioClip _clip;
    private float[] _buffer = new float[SampleRate];

    void Start() {
        Debug.Log(Device + " is " + Type);
        StartRecording();
    }

    void StartRecording() {
        _clip = Microphone.Start(Device, false, 1, SampleRate);
    }

    void CalculateVolume() {
        // Get the microphone buffer
        int position = Microphone.GetPosition(Device);
        Microphone.End(Device);
        _clip.GetData(_buffer, 0);

        if (position > _buffer.Length)
            position = _buffer.Length;

        // Find max volume
        float volume = 0;
        for (int i = 0; i < position; ++i) {
            if (_buffer[i] > volume || _buffer[i] < -volume) {
                volume = (_buffer[i] > 0) ? _buffer[i] : -_buffer[i];
            }
        }

        TempTotal = volume;

        switch (Type) {
            case MicType.Top:    manager.topVolume    = volume; break;
            case MicType.Bottom: manager.bottomVolume = volume; break;
            case MicType.Left:   manager.leftVolume   = volume; break;
            case MicType.Right:  manager.rightVolume  = volume; break;
        }
    }

    void FixedUpdate() {
        if (Microphone.GetPosition(Device) > 0) { // started recording
            CalculateVolume();
            StartRecording();
        }
    }
}
