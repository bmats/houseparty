﻿using UnityEngine;

[RequireComponent(typeof(NavMeshAgent))]
public class PartygoerTrigger : MonoBehaviour {
    public Transform Destination;
    public float TriggerTime = 1.0f;
    public int PartygoerPoints = 1;
    public float DestroyDistance = 1.0f;
    public GameObject PartyIndicator;

    private NavMeshAgent _agent;
    private float _triggerEnterTime = float.MaxValue;
    private bool _triggered = false;

	void Start() {
        _agent = GetComponent<NavMeshAgent>();
	}

    void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag == "Sound Zone") {
            _triggerEnterTime = Time.time;
        }
    }

    void OnTriggerExit(Collider other) {
        if (other.gameObject.tag == "Sound Zone") {
            _triggerEnterTime = float.MaxValue;
        }
    }

    public void Trigger() {
        if (_triggered) return;
        _triggered = true;

        if (_agent == null) {
            // HACK: temporary fix for Start() not being called after being Instantiate()d
            _agent = GetComponent<NavMeshAgent>();
        }

        // Stop wandering
        MonoBehaviour wander = GetComponent<Wander>();
        if (wander != null)
            wander.enabled = false;

        // Go to new destination
        _agent.destination = Destination.position;

        PartyIndicator.SetActive(true);
    }

    void Update() {
        if (Time.time - _triggerEnterTime > TriggerTime) {
            Trigger();
        }

        if (_triggered && !GameManager.GameOver) {
            // Reached the party?
            if ((transform.position - Destination.position).sqrMagnitude < DestroyDistance * DestroyDistance) {
                Debug.Log("Party!");
                GameManager.Score += PartygoerPoints;
                Destroy(gameObject);
            }
        }
    }
}
