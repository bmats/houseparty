﻿using UnityEngine;

public class EnemyTrigger : MonoBehaviour {
    public float TriggerTime = 1.0f;
    public int EnemyPoints = -1;
    public bool GameOver = false;

    private float _triggerEnterTime = float.MaxValue;

	void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag == "Sound Zone") {
            _triggerEnterTime = Time.time;
        }
    }

    void OnTriggerExit(Collider other) {
        if (other.gameObject.tag == "Sound Zone") {
            _triggerEnterTime = float.MaxValue;
        }
    }

	void Update() {
	   if (Time.time - _triggerEnterTime > TriggerTime) {
           Debug.Log("Enemy triggered");
           if (GameOver) {
               GameManager.GameOver = true;
           } else {
               GameManager.Score += EnemyPoints;
           }
           _triggerEnterTime = float.MaxValue; // reset

           ParticleSystem particles = GetComponentInChildren<ParticleSystem>();
           if (particles != null) particles.Play(true);
       }
	}
}
