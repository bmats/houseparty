﻿using UnityEngine;

[RequireComponent(typeof (Light))]
public class DiscoLight : MonoBehaviour {
	const float Speed = 5f;

	public Color[] Colors = {
		Color.blue,
		Color.cyan,
		Color.green,
		Color.magenta,
		Color.red,
		Color.yellow
	};

	private Quaternion _baseRot;
	private float _offset;

    void Start() {
		var light = GetComponent<Light>();
		light.color = Colors[Random.Range(0, Colors.Length)];

		_baseRot = Quaternion.Euler(
			90f + Random.Range(-40f, 40f),
			Random.Range(0f, 360f),
			0);
		_offset = Random.value * Speed;
    }

    void Update() {
		// Fun parametric curves http://jwilson.coe.uga.edu/EMAT6680Su07/Francisco/Assignment10/parametric.html
		transform.rotation = _baseRot * Quaternion.Euler(
			20f * Mathf.Sin((0.5f * Time.time + _offset) * Speed),
			20f * Mathf.Sin((0.4f * Time.time + _offset) * Speed),
			0);
    }
}
