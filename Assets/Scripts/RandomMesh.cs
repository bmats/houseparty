﻿using UnityEngine;

[RequireComponent(typeof (MeshFilter))]
public class RandomMesh : MonoBehaviour {
    public Mesh[] Meshes;

	void Start() {
	   GetComponent<MeshFilter>().sharedMesh = Meshes[Random.Range(0, Meshes.Length)];
	}
}
