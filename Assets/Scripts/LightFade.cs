using UnityEngine;

[RequireComponent(typeof(Light))]
public class LightFade : MonoBehaviour {
    public float FadeTime = 10f;

    private Light _light;
    private float _startIntensity;

    void Start() {
        _light = GetComponent<Light>();
        _startIntensity = _light.intensity;
    }

    void Update() {
        // Fade intensity from full to 0 over the last few seconds
        _light.intensity = (1 - Mathf.Clamp01(
            (Time.timeSinceLevelLoad - (GameManager.Instance.GameTime - FadeTime)) / FadeTime)) * _startIntensity;

        if (_light.intensity <= 0) {
            gameObject.SetActive(false);
        }
    }
}
