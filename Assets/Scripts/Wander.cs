﻿using UnityEngine;

[RequireComponent(typeof(NavMeshAgent))]
public class Wander : MonoBehaviour {
    public BoxCollider bounds;
    public float PileupPatienceTime = 3f;
    public float PileupDistanceThreshold = 1.5f;

    private NavMeshAgent _agent;

    private float _nextCheckTime = 0; // check if we're piled up at this time
    private Vector3 _lastCheckedPosition = Vector3.up * 1000; // nobody will be here starting out

    void Start() {
       _agent = GetComponent<NavMeshAgent>();
       NewDestination();
    }

    void NewDestination() {
        _agent.destination = new Vector3(
            Random.Range(bounds.bounds.min.x, bounds.bounds.max.x),
            0,
            Random.Range(bounds.bounds.min.z, bounds.bounds.max.z));
    }

    void Update() {
        // If the agent has reached the destination, go somewhere new
        if (!_agent.pathPending && _agent.remainingDistance <= _agent.stoppingDistance) {
            NewDestination();
        } else if (Time.time >= _nextCheckTime) {
            // If we have not moved since the last check, go somewhere new (prevent pileups)
            if ((transform.position - _lastCheckedPosition).sqrMagnitude <
                    PileupDistanceThreshold * PileupDistanceThreshold) {
                NewDestination();
                //Debug.Log("Partygoer is impatient");
            }

            // Offset randomly so people don't all change direction at once
            _nextCheckTime = Time.time + Random.Range(PileupPatienceTime, PileupPatienceTime + 1f);
            _lastCheckedPosition = transform.position;
        }
    }
}
