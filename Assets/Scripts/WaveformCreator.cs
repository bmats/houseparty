using UnityEngine;
using Array = System.Array;

class WaveformCreator : MonoBehaviour {
    const int Bins = 256;

    public int HistorySize = 30;
    public int MinFreq = 500, MaxFreq = 10000;
    public float Amplitude = 10f;
    public float LineStartSeparation = 10f, LineEndSeparation = 2f;

    public AudioSource Source = null;
    public Transform LineDestination = null;
    public LineRenderer[] Lines = null;

    /// Number of bins to save to capture between MinFreq and MaxFreq.
    private int _waveformBinCount;
    /// Circular array with history of spectrums.
    private float[][] _waveform;
    /// The current index in _waveform.
    private int _waveformIndex = 0;

    private float[] _buffer = new float[Bins];

    void Start() {
        // Initialize arrays and stuff
        _waveformBinCount = (int) ((float) (MaxFreq - MinFreq) / AudioSettings.outputSampleRate * Bins);
        _waveform = new float[HistorySize][];
        for (int i = 0; i < _waveform.Length; ++i) {
            _waveform[i] = new float[_waveformBinCount];
        }

        foreach (var line in Lines) line.SetVertexCount(HistorySize);
    }

    void FixedUpdate() {
        // Get the new spectrum and save it
        Source.GetSpectrumData(_buffer, 0, FFTWindow.BlackmanHarris);
        Array.Copy(
            _buffer, (int) ((float) MinFreq / AudioSettings.outputSampleRate * Bins),
            _waveform[_waveformIndex], 0,
            _waveformBinCount);

        // Rotate relative to camera
        Quaternion startRotation = Quaternion.AngleAxis(Camera.main.transform.eulerAngles.y, Vector3.up);
        Vector3 rotatedStartPosition = startRotation * transform.position;

        int binsPerLine = _waveformBinCount / Lines.Length;
        for (int l = 0; l < Lines.Length; ++l) {
            LineRenderer line = Lines[l];
            for (int i = 0; i < _waveform.Length; ++i) {
                // Calculate the total volume for this bin
                float total = 0;
                for (int freq = binsPerLine * l; freq < binsPerLine * (l + 1); ++freq)
                    total += Mathf.Abs(_waveform[i][freq]);

                // Get the position on the line corresponding to this index in the history, converting circular index
                int index = i > _waveformIndex ? HistorySize - i + _waveformIndex : _waveformIndex - i;

                float lerpFactor = (float) index / HistorySize;
                Vector3 position = new Vector3(
                    Mathf.Lerp(rotatedStartPosition.x, LineDestination.position.x, lerpFactor),
                    Mathf.Lerp(rotatedStartPosition.y, LineDestination.position.y, lerpFactor * lerpFactor), // curve
                    Mathf.Lerp(rotatedStartPosition.z, LineDestination.position.z, lerpFactor));

                position += startRotation * Vector3.right *
                    Mathf.Lerp(LineStartSeparation, LineEndSeparation, lerpFactor) * (l - Lines.Length * 0.5f);
                position.y += total * Amplitude;

                line.SetPosition(index, position);
            }
        }

        // Advance the history index in circular array
        ++_waveformIndex;
        if (_waveformIndex >= HistorySize) _waveformIndex = 0;
    }
}
