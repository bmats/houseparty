﻿using UnityEngine;

[RequireComponent(typeof (MeshRenderer))]
public class RandomMaterial : MonoBehaviour {
    public Material[] Materials;

	void Start() {
	   GetComponent<MeshRenderer>().material = Materials[Random.Range(0, Materials.Length)];
	}
}
