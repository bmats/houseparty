﻿using UnityEngine;

public class SoundZoneMover : MonoBehaviour {
    public Vector3 DefaultPosition = new Vector3(0, 0, -100);

    void Update() {
        if (GameManager.GameOver) {
            transform.position = DefaultPosition;
            return;
        }

        Vector3 position = GameManager.Instance.SoundPosition;

        Ray ray = Camera.main.ViewportPointToRay(position);
        RaycastHit hit = new RaycastHit();
        if (Physics.Raycast(ray, out hit, 1000, Physics.DefaultRaycastLayers,
                QueryTriggerInteraction.Ignore)) { // ignore myself
            transform.position = hit.point;
            // transform.localRotation = Quaternion.FromToRotation(Vector3.up, hit.normal);
        } else {
            transform.position = DefaultPosition;
        }
    }
}
