﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class TopMic : MonoBehaviour {

    AudioSource source;
    string device;
    int topPort = 3;

    // Use this for initialization
    void Start()
    {
        source = GetComponent<AudioSource>();
        device = Microphone.devices[topPort];
        source.clip = Microphone.Start(device, true, 999, 44100);
        while (!(Microphone.GetPosition(device) > 0)) { }
        source.Play();
    }

    // Update is called once per frame
    void Update()
    {
        if (!(Microphone.IsRecording(device) && source.isPlaying))
            return;
        float[] data = new float[manager.sampleCount];
        source.GetOutputData(data, 0);
        float sum = 0;
        foreach (float f in data)
        {
            sum += Mathf.Abs(f);
        }
        manager.topVolume = sum / manager.sampleCount * 300;
    }
}
