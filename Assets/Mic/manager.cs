﻿using UnityEngine;

public class manager : MonoBehaviour {

    public const int sampleCount = 2048;

    public float VolumeToPixel = 500f;
    public float AverageFactor = 0.1f;
    public const float MaxDiffPerFrame = 100f;

    public static float leftVolume;
    public static float rightVolume;
    public static float topVolume;
    public static float bottomVolume;
    public static float horizontalPosition = -1, verticalPosition = -1;

    public static float x;    //difference on x axis
    public static float y;    //difference on y axis

    //these can all be adjusted
    public float xCorrection = 0f;
    public float yCorrection = 0f;

    void Start() {
        x = Screen.width  * 0.5f;
        y = Screen.height * 0.5f;
    }

    void FixedUpdate() {
        // If absolute position changed from the default, use it
        float newX = horizontalPosition == -1 ? x : horizontalPosition * Screen.width;
        float newY = verticalPosition   == -1 ? y : verticalPosition   * Screen.height;

        // Calculate the difference between sides and use this to move the sound
        float xDiff = rightVolume - leftVolume + xCorrection;
        xDiff *= VolumeToPixel;
        if (xDiff > MaxDiffPerFrame) xDiff = MaxDiffPerFrame;
        else if (xDiff < -MaxDiffPerFrame) xDiff = -MaxDiffPerFrame;
        newX += xDiff;

        float yDiff = topVolume - bottomVolume + yCorrection;
        yDiff *= VolumeToPixel;
        if (yDiff > MaxDiffPerFrame) yDiff = MaxDiffPerFrame;
        else if (yDiff < -MaxDiffPerFrame) yDiff = -MaxDiffPerFrame;
        newY += yDiff;

        if (newX < 0) newX = 0;
        if (newY < 0) newY = 0;
        if (newX > Screen.width)  newX = Screen.width;
        if (newY > Screen.height) newY = Screen.height;

        // Smooth movement
        x = newX * AverageFactor + x * (1 - AverageFactor);
        y = newY * AverageFactor + y * (1 - AverageFactor);
    }
}
