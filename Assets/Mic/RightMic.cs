﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class RightMic : MonoBehaviour {

    AudioSource source;
    string device;
    int rightPort = 2;

    // Use this for initialization
    void Start()
    {
        source = GetComponent<AudioSource>();
        device = Microphone.devices[rightPort];
        source.clip = Microphone.Start(device, true, 999, 44100);
        while (!(Microphone.GetPosition(device) > 0)) { }
        source.Play();
    }

    // Update is called once per frame
    void Update()
    {
        if (!(Microphone.IsRecording(device) && source.isPlaying))
            return;
        float[] data = new float[manager.sampleCount];
        source.GetOutputData(data, 0);
        float sum = 0;
        foreach (float f in data)
        {
            sum += Mathf.Abs(f);
        }
        manager.rightVolume = sum / manager.sampleCount * 300;
    }
}
